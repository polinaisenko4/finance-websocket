import { configureStore } from '@reduxjs/toolkit';
import tickerSlice from "./tickerSlice";

export default configureStore({
    reducer: {
        ticker: tickerSlice,
    },
});