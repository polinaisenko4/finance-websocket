import { createSlice } from '@reduxjs/toolkit';

const tickerSlice = createSlice({
    name: 'ticker',
    initialState: {
        tickers: [],
        ticker: null,
        allTickers:
            {"AAPL" : [], "GOOGL" : [], "MSFT": [], "AMZN": [], "FB": [], "TSLA": []},
        company: {id: "AAPL"}
    },

    reducers: {
        setTicker(state, action) {
            state.tickers = action.payload.map((current) => {
                state.tickers.forEach(prev => {
                    if(current.ticker === prev.ticker) {
                        if(+prev.price > +current.price) {
                            current.color = "negative"
                            current.prevPrice = prev.price
                        }
                        if(+prev.price < +current.price) {
                            current.color = "positive"
                            current.prevPrice = prev.price
                        }
                        if(+prev.price === +current.price) {
                            current.color = "neutral"
                            current.prevPrice = prev.price
                        }
                    }
                })
                return current
            })
        },
        setAllTickers(state, action) {
            const newObj = state.allTickers

            action.payload.forEach((ticket) => {
                for (let key in newObj) {
                    if(!newObj[ticket.ticker]) {
                        return newObj[ticket.ticker]
                    }
                    if (ticket.ticker === key) {
                        return newObj[ticket.ticker].push(ticket)
                    }
                }
            })

            state.allTickers = newObj
        },
        setCompany(state, action) {
            state.company = action.payload
        },
        filterTicker(state, action) {
            state.ticker = state.tickers.filter(el => el.ticker === state.company.id)
        }
    }
});

export const { setTicker, filterTicker, setCompany, setAllTickers } = tickerSlice.actions;

export default tickerSlice.reducer;
