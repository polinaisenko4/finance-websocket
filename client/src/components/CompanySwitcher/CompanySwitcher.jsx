import styles from './companySwitcher.module.scss'
import { typeTicket } from '../../config/index'
import classNames from 'classnames'

const CompanySwitcher = ({ onClick, active, tickers, info }) => {
    const handleClick = (e) => {
        onClick(e)
    }

    return (
        <div className={styles.switcherWrap}>
            {tickers.map((company, i) => {
                return (
                    <div
                        key={i++}
                        className={classNames(styles.currentSwitch, {
                            [styles.isActive]: active === company.ticker,
                        })}
                        onClick={() => handleClick(company.ticker)}
                    >
                        <h2>{typeTicket(company.ticker)}</h2>
                        {(info) &&
                            <p
                            className={classNames({
                                [styles.currentSwitch__positive]:
                                company.color === 'positive',
                                [styles.currentSwitch__negative]:
                                company.color === 'negative',
                                [styles.currentSwitch__neutral]:
                                company.color === 'neutral',
                            })}
                        >
                            {company.price}
                        </p>
                        }
                    </div>
                )
            })}
        </div>
    )
}

export default CompanySwitcher
