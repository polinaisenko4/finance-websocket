import moment from 'moment/moment'
import { Line } from 'react-chartjs-2'

const Charts = ({ company }) => {
    const options = {
        responsive: true,
        plugins: {},
    }

    const data = {
        labels: company.map((el) => {
            const time = el.last_trade_time
            return moment(time).format('h:mm:ss')
        }),
        datasets: [
            {
                label: 'Price',
                data: company.map((el) => el.price),
                borderColor: 'rgb(255, 99, 132)',
                backgroundColor: 'rgba(255, 99, 132, 0.5)',
            },
        ],
    }

    return <Line options={options} data={data} />
}

export default Charts
