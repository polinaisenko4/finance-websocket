import styles from './InfoCompany.module.scss'
import moment from 'moment/moment'
import { typeTicket } from '../../config'
import AboutCompany from '../AboutCompany/AboutCompany'
import { aboutCompany } from '../../config/data'
import classNames from "classnames";

const InfoCompany = ({ companyInfo, children, historyOfCompany, companies }) => {
    const allPrice = historyOfCompany
        ? historyOfCompany.map((el) => +el.price)
        : ''

    return (
        <div className={styles.currentCompany}>
            <div className={styles.currentCompany__header}>
                <h2 className={styles.currentCompany__title}>
                    {typeTicket(companyInfo.ticker)}
                </h2>
                <div className={styles.currentCompany__lastTradeTime}>
                    {moment(companyInfo.last_trade_time).format('lll')}
                </div>
            </div>
            <div className={styles.currentCompany__priceInfo}>
                <h3>{companyInfo.price}</h3>
                <p  className={classNames({
                    [styles.currentCompany__positive]:
                    companyInfo.color === 'positive',
                    [styles.currentCompany__negative]:
                    companyInfo.color === 'negative',
                    [styles.currentCompany__neutral]:
                    companyInfo.color === 'neutral',
                })}>
                    {companyInfo.change}
                </p>
                <p>{companyInfo.change_percent} %</p>
            </div>
            <div className={styles.currentCompany__info}>
                <div>
                    <div className={styles.currentCompany__graph}>{children}</div>
                </div>
                <div className={styles.currentCompany__rightBar}>
                    <div className={styles.currentCompany__infoRow}>
                        <h4>Price</h4>
                        <p>{companyInfo.price}</p>
                    </div>
                    <div className={styles.currentCompany__infoRow}>
                        <h4>Dividend</h4>
                        <p>{companyInfo.dividend}</p>
                    </div>
                    <div className={styles.currentCompany__infoRow}>
                        <h4>Yield</h4>
                        <p>{companyInfo.yield}</p>
                    </div>
                    <div className={styles.currentCompany__infoRow}>
                        <h4>Range</h4>
                        <div>
                            <span>{Math.min(...allPrice)}</span>
                            <span> - </span>
                            <span>{Math.max(...allPrice)}</span>
                        </div>
                    </div>
                </div>
            </div>
            <AboutCompany text={aboutCompany} />
        </div>
    )
}

export default InfoCompany
