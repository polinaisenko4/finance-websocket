import styles from './AboutCompany.module.scss'
import { useSelector } from 'react-redux'

const AboutCompany = ({ text }) => {
    const company = useSelector((state) => state.ticker.company)

    return (
        <>
            {text.map((about) => {
                if (about.id === company.id) {
                    return (
                        <div className={styles.about} key={about.id}>
                            <div className={styles.about__title}>
                                <h4>{`About company`}</h4>
                            </div>
                            <div className={styles.about__answer}>
                                <p>{about.text}</p>
                            </div>
                        </div>
                    )
                }
            })}
        </>
    )
}

export default AboutCompany
