import './App.scss'
import './styles/reset.styles.scss'
import { io } from 'socket.io-client'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
    setTicker,
    setCompany,
    filterTicker,
    setAllTickers,
} from './store/tickerSlice'
import CompanySwitcher from './components/CompanySwitcher/CompanySwitcher'
import InfoCompany from './components/InfoCompany/InfoCompany'

import React from 'react'
import {
    Chart,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js'
import Charts from './components/Charts/Charts'

Chart.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
)
const App = () => {
    const company = useSelector((state) => state.ticker.company)
    const allTickers = useSelector((state) => state.ticker.allTickers)
    const tickers = useSelector((state) => state.ticker.tickers)
    const currentTicker = useSelector((state) => state.ticker.ticker)

    const dispatch = useDispatch()

    useEffect(() => {
        const socketIo = io('http://localhost:4000')
        socketIo.emit('start')

        socketIo.on('ticker', (data) => {
            dispatch(setTicker(data))
            dispatch(filterTicker())
            dispatch(setAllTickers(data))
        })
    }, [dispatch])

    const setActiveCompany = (e) => {
        dispatch(setCompany({ id: e }))
        dispatch(filterTicker())
    }

    return (
        <div className="App">
            <CompanySwitcher
                tickers={tickers}
                active={company.id}
                info={true}
                onClick={setActiveCompany}
            />
            {currentTicker &&
                currentTicker.map((el) => (
                    <div key={el.ticker}>
                        <InfoCompany
                            companies={tickers}
                            companyInfo={el}
                            historyOfCompany={allTickers[company.id]}
                        >
                            <Charts company={allTickers[company.id]} />
                        </InfoCompany>
                    </div>
                ))}
        </div>
    )
}

export default App
